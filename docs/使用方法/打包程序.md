程序如直接打包为exe文件，运行会遇到报错。这是可能因为程序在默认路径找不到ini文件引起的。解决的方法有两种：

1. **把ini文件放到打包的程序文件夹**
   这样程序运行时会根据相对路径查找ini文件，避免找不到默认文件的问题

```python
from DrissionPage import Drission, MixPage

drission = Drission(ini_path=r'.\configs.ini')  # ini文件放在程序相同路径下
page = MixPage(drission=drission)
```

2. **把配置写到程序中，不使用ini文件**

```python
from DrissionPage.config import DriverOptions, SessionOptions
from DrissionPage import Drission, MixPage

do = DriverOptions(read_file=False)
so = SessionOptions(read_file=False)
drission = Drission(driver_or_options=do, session_or_options=so)
page = MixPage(drission=drission)
```

**注意** ，这个时候Drission的两个参数都要输入内容，如果其中一个不需要设置可以输入False，如：

```python
drission = Drission(driver_or_options=do, session_or_options=False)
```