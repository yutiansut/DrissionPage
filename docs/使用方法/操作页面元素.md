# 操作页面元素

d 模式下的 DriverElement 对象可以对浏览器相应元素进行控制。

# 一览表

以下为元素操作方法一览表，然后再详细介绍每个方法。

```python
# 点击元素
click()

# 带偏移量的点击元素
click_at(x, y)

# 右键单击元素
r_click()

# 带偏移量的右键单击元素
r_click_at(x, y)

# 输入内容到元素
input('...')

# 对元素执行 js 脚本
run_script('...')

# 清空元素
clear()

# 对元素截图
screenshot()

# 设置元素某个 property 属性值
set_prop('prop', 'value')

# 设置元素某个 attribute 属性值
set_attr('attr', 'value')

# 删除元素某个 attribute 属性
remove_attr('attr')

# 提交表单
submit()

# 按偏移量拖拽元素
drag(x, y)

# 拖拽元素到某个元素或坐标
drag_to(other_ele_or_location)

# 在元素上悬停鼠标，可带偏移量
hover(x, y)

# 执行下来列表操作，详见下面 Select 类部分
select
```

# 元素操作方法

## click()

此方法用于点击元素。可以选择是否用 js 方式点击，可以在点击失败时自动重试。默认情况下，使用 selenium 原生的点击方法，如果重试超过限定时间，自动改用 js 方式点击。可通过 by_js 参数设置点击方式。  
此设计除了可保证点击成功，还可以用于检测页面上的遮罩层是否消失。遮罩层经常出现在 js 方式翻页的时候，它出现的时候会阻碍 selenium
的原生点击，所以可以通过对其下面的元素不断重试点击，来判断遮罩层是否存在。当然，这个方法是否可行要看具体网页设计。  
而如果直接使用 js 进行点击，则可无视任何遮挡，只要元素在 DOM 内，就能点击得到，这样可以根据须要灵活地对元素进行操作。

注意：使用 js 方式点击时，是不会进行重试的。

参数：

- by_js：是否用 js 方式点击，为 None 时先用 selenium 原生方法点击，重试失败超时后改为 用 js 点击；为 True 时直接用 js 点击；为 False 时即使重试超时也不会改用 js。
- timeout：点击失败重试超时时间，为 None 时使用父页面 timeout 设置。

返回：bool，表示是否点击成功。

```python
# 点击一个元素，重试超时根据所在页面元素而定，都失败就改用 js 点击
ele.click()

# 点击一个元素，重试 5 秒，都失败也不改用 js 点击
ele.click(by_js=False, timeout=5)

# 假设遮罩层出现，ele 是遮罩层下方的元素
ele.click(timeout = 10)  # 不断重试点击，直到遮罩层消失，或到达 10 秒
ele.click(by_js=True)  # 无视遮罩层，直接用 js 点击下方元素
```



## click_at()

此方法用于带偏移量点击元素，偏移量相对于元素左上角坐标。不传入x或y值时点击元素中点。可选择是否用 js 方式点击，但不会进行重试。  
可用于点击一些奇怪的东西，比如用 before 表示的控件。  
点击的目标不一定在元素上，可以传入负值，或大于元素大小的值，点击元素附近的区域。向右和向下为正值，向左和向上为负值。

参数：

- x：相对元素左上角坐标的x轴偏移量
- y：相对元素左上角坐标的y轴偏移量
- by_js：是否用js点击

返回：None

```python
# 点击元素右上方 50*50 的位置
ele.click_at(50, -50)

# 点击元素上中部，x 相对左上角向右偏移50，y 保持在元素中点
ele.click_at(x=50)

# 点击元素中点，和 click() 相似，但没有重试功能
ele.click_at()
```

## r_click()

此方法实现右键单击元素。

无参数。

返回：None

```python
ele.r_click()
```

## r_click_at()

此方法用于带偏移量右键点击元素，用法和 click_at() 相似，但没有 by_js 参数。

参数：

- x：相对元素左上角坐标的x轴偏移量
- y：相对元素左上角坐标的y轴偏移量

返回：None

```python
# 点击距离元素左上角 50*50 的位置（位于元素内部）
ele.r_click_at(50, 50)  
```

## input()

此方法用于向元素输入文本或组合键，也可用于输入文件路径到input元素（文件间用\n间隔）。可选择输入前是否清空元素。   
insure_input 参数为 True 时可自动确保输入正确。该功能是为了应对 selenium 原生输入在某些i情况下会失效的问题。  
接收组合键的时候可接收 selenium 的 Keys 对象的值。组合键要放在一个 tuple 中传入。  
注意：insure_input 为 True 时不能用于接收组合键。  
**Tips:** 有些文本框可以接收回车代替点击按钮，可以直接在文本末尾加上 '\n'。

参数：

- vals：文本值或按键组合
- clear：输入前是否清空文本框
- insure_input：是否确保输入正确，不能用于输入组合键
- timeout：尝试输入的超时时间，不指定则使用父页面的超时时间，只在 insure_input 参数为 True 时生效

返回：bool，表示是否成功输入。insure_input 为 False 时始终返回 True。

```python
# 输入文本
ele.input('Hello world!')

# 输入文本并回车
ele.input('Hello world!\n')

# 输入组合键
from selenium.webdriver import Keys
ele.input((Keys.CONTROL, 'a'), insure_input=False)

# 向上传文本控件输入文本路径（传入多个路径）
ele.input('D:\\test1.txt\nD:\\test2.txt')
```

## run_script()

此方法用于对元素执行 js 代码。

参数：

- script：js 文本
- *args：传入 js 的参数

返回：js 执行的结果

```python
# 用执行 js 的方式点击元素
ele.run_script('arguments[0].click()')
```

## clear()

此方法用于清空元素文本，可使用确保清空的方式，若元素是不可编辑的，返回 None。

参数：

- insure_clear：是否确保清空。为 True 则用 input() 确保值变成 ''，为 False 则用 selenium 元素 clear() 方法

返回：bool，是否清空成功，不能清空的元素返回 None

```python
ele.clear()
```

## screenshot()

此方法用于对元素进行截图。  
如果是图片元素，会自动等待加载结束才截图。  
此方法能自动获取能够使用的文件名，避免重名覆盖原有文件。并返回保存路径。  
保存格式为 png。

参数：

- path：图片保持路径
- filename：图片文件名，不传入时以元素 tag 标签命名

返回：图片的完整路径

```python
path = ele.screenshot(r'D:\tmp', 'img_name')
```

## set_prop()

此方法用于设置元素 property 属性。

参数：

- prop： 属性名
- value： 属性值

返回：bool，是否设置成功

```python
ele.set_prop('value', 'Hello world!')
```

## set_attr()

此方法用于设置元素 attribute 属性。

参数：

- attr：属性名
- value：属性值

返回：bool，是否设置成功

```python
ele.set_attr('href', 'http://www.gitee.com')
```

## remove_attr()

此方法用于删除元素 attribute 属性。

参数：

- attr：属性名

返回：bool，是否删除成功

```python
ele.remove_attr('href')
```

## submit()

此方法用于提交表单，若元素不在表单内，返回 None，否则返回 True。

无参数。

返回：True 或 None

```python
ele.submit()
```

## drag()

此方法用于拖拽元素到相对于当前的一个新位置，可以设置速度，可以选择是否随机抖动。

参数：

- x：x 变化值
- y：y 变化值
- speed：拖动的速度，传入 0 即瞬间到达
- shake：是否随机抖动

返回：bool，表示是否拖动成功

```python
# 拖动当前元素到距离 50*50 的位置，速度为 100，不随机抖动
ele.drag(50, 50, 100, False)
```

## drag_to()

此方法用于拖拽元素到另一个元素上或一个坐标上。

参数：

- ele_or_loc： 另一个元素或坐标元组，接收元素时坐标是元素中点的坐标。可接收 selenium 的 WebElement 或本库的 DriverElement
- speed： 拖动的速度，传入 0 即瞬间到达
- shake： 是否随机抖动

返回：bool，表示是否拖动成功

```python
# 把 ele1 拖拽到 ele2 上
ele1 = page.ele('#div1')
ele2 = page.ele('#div2')
ele1.drag_to(ele2)

# 把 ele1 拖拽到网页 50, 50 的位置
ele1.drag_to((50, 50))
```

## hover()

此方法用于模拟鼠标悬停在元素上，可接受偏移量，偏移量相对于元素左上角坐标。不传入 x 或 y 值时悬停在元素中点。x 和 y 值可接受负值。

参数：

- x：相对元素左上角坐标的x轴偏移量
- y：相对元素左上角坐标的y轴偏移量

返回：None

```python
# 悬停在元素右上方 50*50 的位置
ele.hover(50, -50)

# 悬停在元素上中部，x 相对左上角向右偏移50，y 保持在元素中点
ele.hover(x=50)

# 悬停在元素中点
ele.hover()
```

# Select 类

select 元素的操作较为复杂，因此专门做了一个类用于处理它。每个 DriverElement 都有 select 属性，如果是 select 元素，该属性是一个 Select 类，否则该属性为 False。  
select 元素要操作列表时，实际上是对这个 Select 对象进行操作。

```python
ele.select
```

## 属性

### is_multi

该属性表示当前 select 元素是否多选列表。

```python
ele.select.is_multi
```

### options

该属性以列表形式返回当前 select 元素下所有列表项元素对象，这些对象是 DriverElement。

```python
options = ele.select.options
```

### selected_option

该属性返回第一个被选中的元素对象。

```python
option_ele = ele.select.selected_option
```

### selected_options

该属性以列表形式返回第所有被选中的元素对象。如果是单选列表，返回一个列表

```python
options = ele.select.select.selected_options
```

## 方法

### select()

该方法用于选定或取消选定下拉列表中子元素。Select 类的 \_\_call\_\_() 方法直接调用这个方法，因此可以直接`ele.select()`来替代这个方法。写法更直观。  
当元素是多选列表时，可以接受 list 或 tuple，同时选择多个项。

参数：

- text_value_index：根据文本、值选或序号择选项，若允许多选，传入list或tuple可多选
- para_type：参数类型，可选 'text'、'value'、'index'，默认根据文本选择
- deselect：是否取消选择

返回：是否选择成功

```python
# 根据文本选择下拉列表项
ele.select('text') 

# 根据 value 选择下拉列表项
ele.select(value, 'value')  

# 根据序号选择下拉列表项
ele.select(index, 'index')  

# 选择多个文本项
ele.select(('text1', 'text2'))

# 选择多个 value 项
ele.select(('value1', 'value2'), 'value')

# 选择多个序号
ele.select(('index1', 'index2'), 'index')
```

### deselect()

此方法用于取消选定下拉列表中子元素。当元素是多选列表时，可以接受 list 或 tuple，同时取消选择多个项。

参数：

- text_value_index：根据文本、值选或序号择选项，若允许多选，传入list或tuple可多选
- para_type：参数类型，可选 'text'、'value'、'index'

返回：是否选择成功

```python
# 根据文本取消选择下拉列表项
ele.select.deselect('text') 

# 根据 value 取消选择下拉列表项
ele.select.deselect(value, 'value')  

# 根据序号取消选择下拉列表项
ele.select.deselect(index, 'index')  

# 取消选择多个文本项
ele.select.deselect(('text1', 'text2'))

# 取消选择多个 value 项
ele.select.deselect(('value1', 'value2'), 'value')

# 取消选择多个序号
ele.select.deselect(('index1', 'index2'), 'index')
```

## 多项列表独有功能

### clear()

此方法用于清空多选列表选项。

无参数。

返回：None

```python
ele.select.clear()
```

### invert

此方法用于反选多选列表选项。

无参数。

返回：None

```python
ele.select.invert()
```